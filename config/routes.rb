Rails.application.routes.draw do
  get 'welcome/index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get 'posts/new'
  post "posts" => "posts#create"

  get 'posts/:id' => 'posts#show'

  get 'posts' => 'posts#index'

  get 'posts/:id/edit' => 'posts#edit'
  put 'posts/:id' => 'posts#update'

  delete 'posts/:id' => 'posts#destroy'
end
