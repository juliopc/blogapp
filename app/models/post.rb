class Post < ApplicationRecord
    # attr_accessible :texto, :titulo

    validates :titulo, :presence => true,
                    :length => { :minimum => 5 }
end
