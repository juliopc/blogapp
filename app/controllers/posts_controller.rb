class PostsController < ApplicationController
    def new
        @posts = Post.new
    end

    def create
        @posts = Post.new(post_param)

        if @posts.save
            redirect_to :action => :show, :id => @posts.id
        else
           render 'new'
        end
    end

    def show
        @posts = Post.find(params[:id])
    end

    def index
        @posts = Post.all
    end

    def edit
        @posts = Post.find(params[:id])
    end

    def update
        @posts = Post.find(params[:id])

        if @posts.update(post_param)
            redirect_to :action => :show, :id => @posts.id
        else
            render 'edit'
        end
    end

    def destroy
        @posts = Post.find(params[:id])
        @posts.destroy
        redirect_to :action => :index
    end

    private def post_param
        params.permit(:titulo, :texto)
    end
end